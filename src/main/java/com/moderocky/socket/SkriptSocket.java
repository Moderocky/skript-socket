package com.moderocky.socket;

import ch.njol.skript.Skript;
import ch.njol.skript.SkriptAddon;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;

public class SkriptSocket extends JavaPlugin {

    private static SkriptSocket instance;
    private static SkriptAddon addon;

    public static SkriptSocket getInstance() {
        return instance;
    }

    public static SkriptAddon getAddon() {
        return addon;
    }

    @Override
    public void onEnable() {
        instance = this;
        start();

    }

    @Override
    public void onDisable() {
        instance = null;
        stop();

    }

    private void start() {

        if ((getSkript() != null) && (Skript.isAcceptRegistrations())) {
            addon = Skript.registerAddon(this);
            try {
                addon.loadClasses("com.moderocky.socket", "syntax");
            } catch (IOException e) {
                getLogger().info("**  Skript could not be linked!");
                disablePlugin();
            }
            getLogger().info("**  Skript has been linked!");
            getLogger().info("**  Loading syntaxes!");
        } else {
            getLogger().info("**  Skript was not found!");
            getLogger().info("**  Are you confused, perhaps?");

        }
    }

    private void stop() {
        //Disabley stuff.
    }

    private void disablePlugin() {
        Bukkit.getPluginManager().disablePlugin(this);
    }

    private Plugin getSkript() {
        return Bukkit.getPluginManager().getPlugin("Skript");
    }

}
