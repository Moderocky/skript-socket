package com.moderocky.socket.api;

import com.moderocky.socket.SkriptSocket;
import com.moderocky.socket.event.SocketReceiveEvent;
import org.bukkit.Bukkit;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.stream.Collectors;

public class Receiver implements Runnable {

    private ServerSocket serverSocket;
    private int port;
    private boolean running;
    private int timer;
    private Thread thread;

    public Receiver(int port) {
        this.port = port;
        try {
            serverSocket = new ServerSocket(port);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        running = true;
        timer = 0;
        tick();

    }

    public void close() {
        running = false;
    }

    public void setThread(Thread thread) {
        this.thread = thread;
    }

    private void tick() {
        try {
            timer = 0;
            if (serverSocket == null)
                serverSocket = new ServerSocket(port);
            Socket sock = serverSocket.accept();
            BufferedReader br = new BufferedReader(new InputStreamReader(sock.getInputStream()));
            String str = br.lines().collect(Collectors.joining(System.lineSeparator()));

            Bukkit.getScheduler().runTask(SkriptSocket.getInstance(), () -> Bukkit.getServer().getPluginManager().callEvent(new SocketReceiveEvent(sock.getInetAddress().getHostAddress(), sock.getPort(), str)));
        } catch (Exception e) {
            timer++;
            if (timer > 20)
                running = false;
        }
        if (running)
            tick();
        else {
            Handler.map.keySet().forEach(key -> {
                if (Handler.map.get(key) == this)
                    Handler.map.remove(key);
            });
            thread.suspend();
        }

    }

}
