package com.moderocky.socket;

import net.md_5.bungee.api.plugin.Plugin;

public class Proxy extends Plugin {

    @Override
    public void onEnable() {

        getLogger().info("**                                            ");
        getLogger().info("**     Hello there!                           ");
        getLogger().info("**                                            ");
        getLogger().info("**   This addon was only designed to          ");
        getLogger().info("**   work with Skript, not on a bungeecord    ");
        getLogger().info("**   server. :(                               ");
        getLogger().info("**                                            ");
        getLogger().info("**   I might consider standalone support      ");
        getLogger().info("**   at a later date, but for now,            ");
        getLogger().info("**   please behave normally and use           ");
        getLogger().info("**   a Bukkit server. :)                      ");
        getLogger().info("**                                            ");

    }
}
