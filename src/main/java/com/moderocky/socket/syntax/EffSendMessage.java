package com.moderocky.socket.syntax;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Effect;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import com.moderocky.socket.SkriptSocket;
import com.moderocky.socket.api.Handler;
import com.moderocky.socket.api.Receiver;
import com.moderocky.socket.event.SocketReceiveEvent;
import com.moderocky.socket.event.SocketSendEvent;
import org.bukkit.Bukkit;
import org.bukkit.event.Event;

import javax.annotation.Nullable;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;

@Name("Send Socket Message")
@Description({
        "Sends a socket message to an open receiver on an ip/port. No result is generated, so any reply will have to be handled manually."})
@Examples({
        "send message \"hello there!\" to ip \"1.2.3.4:12345\"",
        "send message \"general kenobi!\" to ip \"127.0.0.1:2019\"",
        "send message \"i don't like sand\" to ip \"69.69.69.69\" and port 3000",
        "send message \"it's treason, then\" to port 3000"
})
@Since("0.0.1")
public class EffSendMessage extends Effect {

    @Nullable
    private Expression<String> ipExpr;
    @Nullable
    private Expression<Number> portExpr;
    private Expression<String> messageExpr;


    static {
        Skript.registerEffect(EffSendMessage.class,
                "send [message[s]] %strings% to [socket with] ip %string%",
                "send [message[s]] %strings% to [socket with] ip %string% (and|with) port %number%",
                "send [message[s]] %strings% to [socket with] port %number%"
        );
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean init(Expression<?>[] expr, int matchedPattern, Kleenean arg2, SkriptParser.ParseResult result) {
        if (matchedPattern < 2)
            ipExpr = (Expression<String>) expr[1];
        if (matchedPattern == 1)
            portExpr = (Expression<Number>) expr[2];
        if (matchedPattern == 2)
            portExpr = (Expression<Number>) expr[1];

        messageExpr = (Expression<String>) expr[0];

        return true;
    }

    @SuppressWarnings("null")
    @Override
    protected void execute(Event event) {
        Integer port = null;
        String ip = null;
        if (ipExpr != null && ipExpr.getSingle(event).matches("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]):[0-9]+$")) {
            ip = ipExpr.getSingle(event).split(":")[0];
            port = Integer.valueOf(ipExpr.getSingle(event).split(":")[1]);
        } else if (ipExpr != null && portExpr != null && ipExpr.getSingle(event).matches("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$")) {
            ip = ipExpr.getSingle(event);
            port = portExpr.getSingle(event).intValue();
        } else if (portExpr != null) {
            ip = "127.0.0.1";
            port = portExpr.getSingle(event).intValue();
        }

        String[] messages = messageExpr.getAll(event);

        if (ip == null || port == null)
            return;

        for (String message : messages) {

            Boolean failed;
            try {
                Socket socket = new Socket(ip, port);
                socket.getOutputStream().write(message.getBytes());
                failed = false;
            } catch (UnknownHostException e) {
                Bukkit.getLogger().log(Level.WARNING, "Unable to link to the receiver socket.");
                failed = true;
            } catch (IOException e) {
                Bukkit.getLogger().log(Level.WARNING, "Couldn't get I/O for the connection to the receiver.");
                failed = true;
            }

            final String h = ip;
            final int p = port;
            final boolean b = failed;

            Bukkit.getScheduler().runTask(SkriptSocket.getInstance(), () -> Bukkit.getServer().getPluginManager().callEvent(new SocketSendEvent(h, p, b, message)));

        }

    }

    @Override
    public String toString(@Nullable Event event, boolean debug) {
        String message = "send messages " + messageExpr.toString(event, debug) + " to socket";
        if (ipExpr != null) {
            message = message + " with ip "  + ipExpr.toString(event, debug);
        }

        if (portExpr != null) {
            message = message + " with port "  + portExpr.toString(event, debug);
        }
        return message;
    }
}
