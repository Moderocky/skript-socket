package com.moderocky.socket.syntax;

import ch.njol.skript.Skript;
import ch.njol.skript.lang.util.SimpleEvent;
import ch.njol.skript.registrations.EventValues;
import ch.njol.skript.util.Getter;
import com.moderocky.socket.event.SocketSendEvent;

public class EvtSocketSend {

    static {
        Skript.registerEvent("Socket Message Sent", SimpleEvent.class, SocketSendEvent.class, "socket message send[ing]")
                .description("Called when the server sends a socket message. The result or success of the message may not be known at this time.")
                .examples(
                        "on socket message send:",
                        "    broadcast event-string")
                .since("1.0.0");
        EventValues.registerEventValue(SocketSendEvent.class, String.class, new Getter<String, SocketSendEvent>() {
            @Override
            public String get(SocketSendEvent e) {
                return e.getMessage();
            }
        }, 0);
        EventValues.registerEventValue(SocketSendEvent.class, String.class, new Getter<String, SocketSendEvent>() {
            @Override
            public String get(SocketSendEvent e) {
                return e.getIp();
            }
        }, 0);
        EventValues.registerEventValue(SocketSendEvent.class, Number.class, new Getter<Number, SocketSendEvent>() {
            @Override
            public Number get(SocketSendEvent e) {
                return e.getPort();
            }
        }, 0);
        EventValues.registerEventValue(SocketSendEvent.class, Boolean.class, new Getter<Boolean, SocketSendEvent>() {
            @Override
            public Boolean get(SocketSendEvent e) {
                return e.hasFailed();
            }
        }, 0);
    }
}
