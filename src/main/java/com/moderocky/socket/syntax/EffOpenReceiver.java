package com.moderocky.socket.syntax;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Effect;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import com.moderocky.socket.api.Handler;
import com.moderocky.socket.api.Receiver;
import org.bukkit.Bukkit;
import org.bukkit.event.Event;

import javax.annotation.Nullable;

@Name("Open Receiver Socket")
@Description({
        "Opens a socket capable of receiving messages. This will trigger the 'Socket Receive Event' whenever a message is received.\nThe server IP and socket port are necessary for sending a message to it. In order to open in on a random port, provide the port '0'. This is unwise.\nIf the port is unavailable, it will continually throw silenced exceptions and basically do nothing until it times out."})
@Examples("open receiver socket on port 20000")
@Since("0.0.1")
public class EffOpenReceiver extends Effect {

    private Expression<Number> portExpr;

    static {
        Skript.registerEffect(EffOpenReceiver.class,
                "open [a] receiver socket on port %number%"
        );
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean init(Expression<?>[] expr, int matchedPattern, Kleenean arg2, SkriptParser.ParseResult result) {
        portExpr = (Expression<Number>) expr[0];

        return true;
    }

    @SuppressWarnings("null")
    @Override
    protected void execute(Event event) {
        int port = portExpr.getSingle(event).intValue();
        String id = Bukkit.getIp() + port;

        if (Handler.isAssigned(id))
            return;

        Receiver receiver = new Receiver(port);
        Handler.map.put(id, receiver);
        Thread thread = new Thread(new Receiver(port));
        receiver.setThread(thread);
        thread.start();

    }

    @Override
    public String toString(@Nullable Event event, boolean debug) {
        String message = "open a receiver socket";

        if (portExpr != null) {
            message = message + " on port "  + portExpr.toString(event, debug);
        }
        return message;
    }
}
