package com.moderocky.socket.syntax;

import ch.njol.skript.Skript;
import ch.njol.skript.lang.util.SimpleEvent;
import ch.njol.skript.registrations.EventValues;
import ch.njol.skript.util.Getter;
import com.moderocky.socket.event.SocketReceiveEvent;

public class EvtSocketReceive {

    static {
        Skript.registerEvent("Socket Message Sent", SimpleEvent.class, SocketReceiveEvent.class, "socket message receiv(e|ing)")
                .description("Called when the server receives a socket message. This contains the message, ip and port.")
                .examples(
                        "on socket message receive:",
                        "    broadcast event-string")
                .since("1.0.0");
        EventValues.registerEventValue(SocketReceiveEvent.class, String.class, new Getter<String, SocketReceiveEvent>() {
            @Override
            public String get(SocketReceiveEvent e) {
                return e.getMessage();
            }
        }, 0);
        EventValues.registerEventValue(SocketReceiveEvent.class, String.class, new Getter<String, SocketReceiveEvent>() {
            @Override
            public String get(SocketReceiveEvent e) {
                return e.getIp();
            }
        }, 0);
        EventValues.registerEventValue(SocketReceiveEvent.class, Number.class, new Getter<Number, SocketReceiveEvent>() {
            @Override
            public Number get(SocketReceiveEvent e) {
                return e.getPort();
            }
        }, 0);
    }
}
