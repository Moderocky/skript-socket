package com.moderocky.socket.syntax;

import ch.njol.skript.classes.ClassInfo;
import ch.njol.skript.classes.Converter;
import ch.njol.skript.classes.Parser;
import ch.njol.skript.lang.ParseContext;
import ch.njol.skript.registrations.Classes;
import ch.njol.skript.registrations.Converters;

import java.net.Socket;

public class Types {

    static {

        Classes.registerClass(new ClassInfo<>(Socket.class, "socket")
                .user("socket?s?")
                .name("Socket")
                .description("A socket. This is one end of a channel that can send or receive messages.")
                .since("1.0.0")
                .parser(new Parser<Socket>() {
                    @Override
                    public boolean canParse(ParseContext context) {
                        return true;
                    }

                    @Override
                    public String toString(Socket socket, int flags) {
                        return socket.getInetAddress() + ":" + socket.getPort();
                    }

                    @Override
                    public String toVariableNameString(Socket socket) {
                        return socket.getInetAddress() + ":" + socket.getPort();
                    }

                    @Override
                    public String getVariableNamePattern() {
                        return "\\S+";
                    }

                    @Override
                    public Socket parse(String s, ParseContext context) {
                        if (!s.matches("\\d+.\\d+.\\d+.\\d+:\\d+"))
                            return null;
                        String ip = s.split(":")[0];
                        int port = Integer.valueOf(s.split(":")[1]);
                        try {
                            return new Socket(ip, port);
                        } catch (Exception e) {
                            return null;
                        }
                    }
                }));

        Converters.registerConverter(Socket.class, String.class, (Converter<Socket, String>) socket -> socket.getInetAddress() + ":" + socket.getPort());

    }
}
