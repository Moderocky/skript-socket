package com.moderocky.socket.event;


import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class SocketSendEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    private String ip;
    private int port;
    private boolean failed;
    private String message;

    public SocketSendEvent(String ip, int port, boolean failed, String message) {
        this.ip = ip;
        this.port = port;
        this.message = message;
        this.failed = failed;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public String getMessage() {
        return message;
    }

    public boolean hasFailed() {
        return failed;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
}
