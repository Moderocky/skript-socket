package com.moderocky.socket.event;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class SocketReceiveEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    private String ip;
    private int port;
    private String message;

    public SocketReceiveEvent(String ip, int port, String message) {
        this.ip = ip;
        this.port = port;
        this.message = message;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
}
